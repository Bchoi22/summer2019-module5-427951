<?php
session_start();

if (isset($_POST['fileName'])){
    $file = $_POST['fileName'];
    if (empty($file)){  #if the variable is empty
        echo "Error: You did not enter a file to delete.";
      exit;
    }
    if (!preg_match('/^[\w_\.\-]+$/', $file)){ #if the file name is invalid
        echo "Error: Invalid Filename";
        exit;
    }
    $username = $_SESSION['username'];#situating variables for unlink
    $path = sprintf("/srv/%s/%s", $username, $file);
    $type = new finfo(FILEINFO_MIME_TYPE);
    $mime = $type->file($path);

    header("Content-Type: ".$mime);
    if (!file_exists($path)){
        header("Location: delete_not_success.html");
        //echo "Error: File does not exist";
        //exit;
    }
    if (!unlink($path)){
        //echo "Error: File could not be deleted";
        header("Location: delete_not_successful.html");
        exit;
    } else{
        //echo "Deletion Successful!";
        header("Location: delete_success.html");
        exit;
    }
}
?>