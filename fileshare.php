<!DOCTYPE HTML>

<h3>Files in this folder</h3>
<?php
session_start();
$afilepath = sprintf("/srv/%s", $_SESSION['username']);
$dir = opendir($afilepath);

// this php code referenced from https://www.w3schools.com/php
if (is_resource($dir)){
    while (($file = readdir($dir)) !== false){
	  //echo "<a href='$afilepath/$file'>'$file'</a>"."<br>";
	  echo "filename: " . $file . "<br>";
	}
closedir($dir);
}
?>

<br><form action = 'viewfile.php' method = 'GET'>
<?php
$dir = opendir($afilepath);
while (($file = readdir($dir)) !== false) {
?>
	<input type="radio" name="filename" value=<?php echo $file; ?>> <?php echo $file ?> <br>
<?php
}
closedir($dir);
?>
<br><input type = 'submit' value = 'View'><br><br><br><br>
</form>


<h3>Upload a file</h3>
<form enctype="multipart/form-data" action="uploader.php" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" /><br><br><br>
	</p>
</form>

<h3>View a file</h3>
<form action="viewfile.php" method="GET">
    <label>Input filename to view: <input type="text" name="filename"/></label>
    <input type="submit" value="View"/><br><br><br>
</form>

<h3>Delete a file</h3>
<form name="delete" action="deleter.php" method="POST">
    <p>
        <input type="text" name="fileName" id="fileName"/>
        <input type="submit" name="delete" value="Delete File"/>
    </p>
</form>

<br><br><form action = "logout.php">
<input type = 'submit' value = 'Logout'> 
